import 'package:flutter/material.dart';
import 'package:university_details/models/university.dart';
import 'package:university_details/screens/second_screen.dart';
import 'package:university_details/utilities/api.dart';
import 'package:http/http.dart' as http;

class FirstScreen extends StatelessWidget {
  FirstScreen({Key? key}) : super(key: key);
  final Api api = Api();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("University Details"),
      ),
      body: FutureBuilder<List<University>>(
        future: api.fetchUniversities(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return UniversityList(universities: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class UniversityList extends StatelessWidget {
  const UniversityList({Key? key, required this.universities})
      : super(key: key);
  final List<University> universities;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: universities.length,
      itemBuilder: ((context, index) => ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          SecondScreen(university: universities[index])));
            },
            title: Text(
              universities[index].name,
            ),
            subtitle: Text(
              "Country: " + universities[index].country,
            ),
          )),
    );
  }
}
