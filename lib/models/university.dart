class University {
  final String country;
  final String name;
  final String website;
  const University({
    required this.name,
    required this.country,
    required this.website,
  });
  factory University.fromJson(Map<String, dynamic> json) {
    return University(
      name: json['name'] ?? "Unknown",
      country: json['country'] ?? "Unknown",
      website: json['web_pages'][0] ?? "Unknown",
    );
  }
}
