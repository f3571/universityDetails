import 'package:flutter/material.dart';
import 'package:university_details/models/university.dart';
import 'package:url_launcher/url_launcher.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key, required this.university}) : super(key: key);
  final University university;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("University Details"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(children: [
              Text("Name: ",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  )),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Text(
                    university.name,
                    overflow: TextOverflow.fade,
                    style: const TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ]),
            const SizedBox(
              height: 10,
            ),
            Row(children: [
              Text("Country: ",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  )),
              Text(
                university.country,
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
            ]),
            TextButton.icon(
              icon: const Icon(Icons.launch),
              onPressed: () {
                launch(university.website);
              },
              label: const Text("Website"),
            )
          ],
        ),
      ),
    );
  }
}
