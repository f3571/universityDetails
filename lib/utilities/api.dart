import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:university_details/models/university.dart';

class Api {
  Future<List<University>> fetchUniversities(http.Client client) async {
    final response = await client.get(Uri.parse(
        'http://universities.hipolabs.com/search?country=United+States'));
    return compute(getUniversities, response.body);
  }

  List<University> getUniversities(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<University>((json) => University.fromJson(json)).toList();
  }
}
